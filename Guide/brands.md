## Brands

Brands are the trademarks of the items you will repair. In the case of a musical instrument repair shop, this could be Roland, Yamaha, etc. For a phone repair shop, it could be Apple, Samsung, etc. For a home appliance repair shop, it could be Indesit, Whirlpool, etc.

When you create a new brand, the **Name** field is mandatory. You can also optionally include the brand logo and indicate if your workshop is an authorized center for that brand.

**Creating a New Brand**

1. Navigate to the **Brands** page.
2. Click the **New Brand** button.
3. Enter the brand **Name**.
4. (Optional) Upload the brand logo.
5. (Optional) Select the **Authorized Center** checkbox if your workshop is authorized for that brand.
6. Click the **Save** button.

**Additional Information**

* You can create and manage multiple brands.
* Edit or delete existing brands at any time.
* Associate brands with specific repairs for better organization.
* Generate reports based on brands to analyze trends.

