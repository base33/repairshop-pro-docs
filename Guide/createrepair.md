## Create a new repair 

While seemingly simple to use, it's the most complex page in the system due to the behind-the-scenes functionalities that streamline data entry, repair management, and technician workflow.

![customer profile](../images/new_repair.png)


**Creating a Repair**

Creating a repair involves filling out a comprehensive form. Here's a breakdown of the key steps:

1. **Select Customer:**
    * Choose the customer bringing in the item for repair using a dropdown menu.
    * If the customer has one or multiple companies associated with them, a second dropdown menu will appear for selecting the specific company.
2. **Select Brand:**
    * Choose the brand of the item being repaired. If the brand hasn't been created yet, you can do so by clicking the "+" button next to the dropdown menu.
3. **Select Item:**
    * Choose the specific item being repaired from the dropdown menu. Similar to brands, you can create a new item on the fly by clicking the "+" button.
4. **Enter Serial Number:** Enter the serial number of the item being repaired.
5. **Select Priority:** Choose the priority level for the repair.
6. **Select Warranty:** Select the warranty that applies to the repair.

**Accessories**

In the "Accessories" tab, you can list any accessories received along with the item for repair.

**Notes**

The "Notes" tab provides four text fields for detailed information:

* **Customer Description:** Briefly describe the customer's reported issue with the item.
* **Technician Notes:** Add your personal notes regarding the repair.
* **Diagnosis:** Document the diagnosed cause of the problem (Usually compiled later, not viisible to the customer).
* **Repair Notes:** Record details of the repair for future reference, especially on similar items or future repairs on the same item (Usually compiled later, not visible to the customer).

**Assigning the Repair**

Under the "Assignment" section, choose the technician to be assigned the repair and tick the "Is Collected" checkbox to confirm physical possession of the item.

**Saving the Repair**

Click "Save" to finalize the creation of the repair request.

!> **What Happens Behind the Scenes?**

* **Confirmation Email:** RPSPRO automatically sends an email to the customer confirming receipt of the item.
* **Technician Notification:** The assigned technician receives an email notification about the repair request.

## What Happens Next?

From the view page of the repair you just created, you can print:

* **The receipt to give to the customer:** This receipt summarizes the repair details, such as the customer, the item, the priority, and the warranty.
* **The label to attach to the item before bringing it to the lab:** The label contains all the data that identifies the repair and facilitates the processing.