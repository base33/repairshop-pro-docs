# Customer Dashboard

Upon logging into the website, customers are greeted with a dashboard that offers them a comprehensive view of their repair history, expenses, and more. One of the key features of this dashboard is the ability to manage repair estimates directly.

## Features

- **Repair List**: Customers can view a list of all their repairs, providing a clear history of the services they've utilized.
- **Expense Overview**: The dashboard offers insights into how much the customer has spent on repairs, allowing for easy budget tracking.
- **Estimate Management**: The most crucial functionality is the ability for customers to accept or reject estimates. This empowers customers with decision-making capabilities and streamlines the repair process for both parties.

### Accepting or Rejecting Estimates

- **Direct Actions**: Within their dashboard, customers can directly accept or reject repair estimates. This process is designed to be intuitive, ensuring a smooth experience.
- **Notification System**: Customers receive notifications when a new estimate is ready for their review, ensuring they are always up to date.

The customer dashboard is designed to enhance the repair service experience, offering transparency, control, and convenience right at the customers' fingertips.

![customer profile](../images/customer_profile.png)