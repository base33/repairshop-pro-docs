## Customers

Customers are the individuals or businesses that bring their items to you for repair.

**Customer Types**

There are two types of customers:

* **Private:** Individual customers.
* **Business:** Businesses or organizations.

![customer_list](../Images/customers_list.png)

**Creating a Company**
![customer_list](../Images/companies_list.png)
Before creating a business customer, you must first create the company they belong to. RPSPRO is integrated with OPENAPI, so if your customer is an Italian company, you can simply enter their VAT number to automatically retrieve all the necessary data (this also verifies that the VAT number is valid!).

The following information is required for a company:

* **VAT number**
* **Name**
* **Fiscal code**
* **SDI**
* **Email**
* **PEC**
* **Mobile phone**
* **Phone number**
* **Website**
* **Address**

**Creating a Customer**

When you create a customer, several things happen:

1. You must fill out a form with the following information:
    * **Name**
    * **Surname**
    * **Fiscal code**
    * **Place of birth**
    * **Date of birth**
    * **Customer type:** private or business (this information is required for GDPR compliance and for issuing the necessary documents)
2. You must provide the following additional information:
    * **Email**
    * **Mobile phone number**
    * **Company** (if the customer is a business, select the company you created previously)
3. You must provide the customer's full address and choose the method by which they want to be contacted (email, WhatsApp).
4. You can upload the customer's documents, such as their authorization for data processing (which we will discuss later).

!> **What Happens Behind the Scenes**

When you save and create a customer, the system creates a local user and a dedicated profile for the customer. At the same time, a welcome email is sent to the customer with instructions on how to log in to the website and check the status of their repairs.