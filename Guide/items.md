## Items

Items are the individual items you will be repairing. They should not be confused with the repairs themselves, which will be discussed later.

**Creating an Item**

When creating an item, you can specify the following:

* **Name:** A descriptive name for the item.
* **Model:** The specific model of the item.
* **Brand:** The brand of the item (created previously).
* **Item Type:** The category the item belongs to (created previously).

**Additional Information**

* You can create and manage multiple items.
* Edit or delete existing items at any time.
* Associate items with specific repairs to track repair history.
* Generate reports based on items to analyze repair trends.
* Upload relevant documents for each item, such as service manuals, user manuals, software, and reference images.

**Examples**

* **Musical Instrument Repair Shop:**
    * Name: Stratocaster
    * Model: American Performer
    * Brand: Fender
    * Item Type: Electric Guitar
* **Phone Repair Shop:**
    * Name: iPhone 15
    * Model: Pro Max
    * Brand: Apple
    * Item Type: Smartphone
