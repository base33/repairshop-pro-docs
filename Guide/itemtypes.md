## Item Types

Item types define the categories of items you repair.  Think of them as macro categories for grouping similar repairs. 

Here are some examples of item types for different repair shops:

* **Musical Instrument Repair Shop:** Electric Guitar, Bass Amplifier, Guitar Effect, Synthesizer
* **Phone Repair Shop:** Smartphone, Accessory
* **Home Appliance Repair Shop:** Washing Machine, Microwave, Television

When creating a new item type, you only need to specify the **Name**.  The list view of all item types displays the total number of repairs associated with each type. 

**Creating a New Item Type**

1. Navigate to the **Item Types** page.
2. Click the **New Item Type** button.
3. Enter a descriptive **Name** for the item type.
4. Click the **Save** button.

**Additional Information**

* You can create and manage multiple item types as needed.
* Edit or delete existing item types at any time.
* Associating item types with specific repairs helps organize your data.
* Generate reports based on item types to identify repair trends.
