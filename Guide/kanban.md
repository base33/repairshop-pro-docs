# Kanban Page

The Kanban page provides the ability to see all repairs organized visually, offering a comprehensive overview at a glance. Repairs can be filtered by various criteria for efficient management and tracking.

![kanban](../Images/repair_kanban.png)

## Features

- **Visual Organization**: Repairs are displayed in a Kanban-style board, allowing for easy visualization of the repair workflow.
- **Filtering Options**: You have the flexibility to filter repairs based on:
  - Technician: View repairs assigned to specific technicians.
  - User: Filter repairs by the user who logged or is managing the repair.
  - Customer Note: Find repairs based on notes or keywords from customer communications.
  - Brand: Organize repairs by the brand of the item.
  - Model: Filter repairs by the model of the item being repaired.

## Changing Repair Status

- **Easy Status Updates**: Changing the status of a repair is as simple as dragging it from one column to another. This intuitive action allows for quick updates and ensures that the repair process flows smoothly from one stage to the next.

This Kanban system is designed to streamline the management of repairs, making it easier to track progress and manage workloads efficiently.
