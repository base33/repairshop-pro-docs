## Single Repair View

The Single Repair View is the page that technicians should always keep an eye on. It contains all the summarized data of the customer and their item, allowing you to monitor the repair and payment status, communicate with the customer, and much more!

![repair view 1](../images/repair_one.png)

![repair view 2](../images/repair_two.png)

![repair logs](../images/repair_logs.png)

## How it Works

The first part of the page shows a **real-time updated graph** that displays the duration of each work phase since the repair was created.

Immediately below, there is a section with **4 action buttons**:

- **Send email**: Communicate with the customer using pre-set templates (e.g., sending an estimate or a job completion email) or regular emails. A log of all sent emails is visible at the bottom of the page.
- **Calls**: When you call a customer or receive a call from them, you can keep track by writing a summary of what was discussed during the call. This is also logged.
- **Print**: Print the receipt when the customer delivers the item to be repaired, print the label to be included with the item and that will accompany it throughout its life in the lab, and then print the form with the authorization to process personal data and the proforma invoices in two versions, with aggregated VAT and with separate VAT.

![repair send email](../images/repair_send_email.png)

![repair send email](../images/repair_print.png)


Below the buttons, you will find the customer's repair details with **2 important action buttons**:

1. One to change the repair status, selectable from: open, initial inspection, quote creation, quote sent, quote approved, quote rejected, ordering parts, waiting for parts, repair, quality check, ready, delivery, and closed.
2. The other button is used to change the payment status: unpaid, partial, paid.

## Update Notes

In this section, you can update your notes and comments as the repair progresses. This will be useful for documenting the work done for the customer and for the future when you will be performing the same type of repair.

## Manage Parts

Here you can enter the spare parts used, including the type, brand, model, supplier, quantity, purchase price, and selling price to the customer. You can also apply a discount if you wish.

The total of the parts entered here will contribute to the creation of the estimate, so you will also need to enter the labor cost.

## Logs

The logs table displays the logs that are automatically created at each work progress, from when the ticket is created until it is closed. Logs can be public or private. Public logs can be viewed by the customer in the history of their repair.

!> **What Happens Behind the Scenes?**

When a repair is created, the customer can view it in their profile to follow its progress. When, during our work, we have entered the spare parts that we will use and changed the repair status from "Quote Creation" to "Quote Sent", we will send an email to the customer. The customer will then find the possibility to accept or reject the estimate in their profile.

If they accept the estimate, they will receive a confirmation and start of work email. At the same time, an email will be sent to the technician so that they can start work.

If they reject the estimate, they will receive an email inviting them to collect their item. An email will also be sent to the technician who will then change the status to "READY".
