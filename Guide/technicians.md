## Technicians

Technicians are the individuals that perform repairs.

**Creating a Technician**

When you create a technician, several things happen:

1. You must fill out a form with the following information:
    * **Name**
    * **Surname**
    * **Fiscal code**
    * **Place of birth**
    * **Date of birth**

2. You must provide the following additional information:
    * **Email**
    * **Mobile phone number**
3. You must provide the technician's full address and choose the method by which they want to be contacted (email, WhatsApp).
4. You can upload the technician's documents, such as their authorization for data processing (which we will discuss later).

!> **What Happens Behind the Scenes**

When you save and create a technician, the system creates a local user and a dedicated profile for the technician. At the same time, a welcome email is sent to the technician with instructions on how to log in to the website and check the status of their repairs.