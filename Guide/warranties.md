## Warranties

Warranties are a vital part of any repair business. They protect your customers and give them peace of mind knowing that their repairs are covered.

**Types of Warranties**

There are two main types of warranties:

* **Manufacturer's warranty:** This is the warranty provided by the manufacturer of the item. It usually covers defects in materials and workmanship.
* **Workshop warranty:** This is the warranty provided by your workshop. It can cover defects in materials, workmanship, and labor.

**Creating a Warranty**

To create a warranty, you must specify the following:

* **Name:** A descriptive name for the warranty.
* **Type:** Manufacturer's warranty or workshop warranty.
* **Duration:** The length of the warranty in days, months, or years.
* **Description:** A detailed description of what the warranty covers.

**Additional Information**

* You can create and manage multiple warranties.
* Edit or delete existing warranties at any time.
* Associate warranties with specific repairs to track warranty claims.
* Generate reports based on warranties to analyze warranty trends.